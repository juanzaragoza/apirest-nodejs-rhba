exports.responsePaginatedSucessfull = function(response, results){
  return response.json(
    {
      code: 200,
      limit: 10,
      page: 1,
      rows: results
    }
  );	
}

exports.responseSucessfull = function(response,results){
  return response.json(results);
}

exports.responseNotFound = function (response, message){
  return response.status(400).json({
    code: 404,
    error: message
  });
}

exports.responseInternalError = function (response,error){
  return response.status(500).send({
    code: 500,
    error: error
  });
}

exports.responseUnauthorizedAccess = function (response, message){
  return response.status(401).json({
    code: 401,
    error: message
  });
}

exports.responseBadRequest = function (response, message){
  return response.status(400).json({
    code: 400,
    error: message
  });
}
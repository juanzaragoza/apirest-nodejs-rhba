# API NodeJS Scaffolding

Estructura de directorios basica para iniciar un proyecto en Node.js y Express.js. 
El proyecto contiene

* Codigo autodocumentado con apiDoc
* Autenticación y seguridad con JWT
* ORM basico Sequelize

## Requerimientos

* NodeJS v6.8.1
* grunt-cli@1.2.0 

## Instalación

Instalar Grunt

		npm install -g grunt-cli

Ejecutar en el raiz del proyecto

		npm install

Copiar el archivo ´config/config.default.json´ a ´config/config.json´ y configurarlo.

Generar el apidoc

		./node_modules/apidoc/bin/apidoc -i router/ -o apidoc/
		
Ejecutar aplicación

		export NODE_ENV=prod && node app.js

## Pruebas

Desde un navegador, ingrese a la home de la aplicacion junto con el puerto configurado en ´config/config.json´. Por ejemplo: http://localhost:PORT/apidoc/





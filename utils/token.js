var debug = require('debug')('app:utils:' + process.pid),
    _ = require("underscore"),
    config = include("config/config"),
    jsonwebtoken = require("jsonwebtoken");

exports.create = function (data) {

    if (_.isEmpty(data)) {
        return false;
    }

    return generate(data);

};

exports.verify = function(token) {

    try{
        
        var user = jsonwebtoken.verify(token, config.secret);

        delete user.iat;
        delete user.exp;
        return generate(user);

    } catch (e) {
        return false;
    }
    
}

exports.get = function(token) {

    try{
        return jsonwebtoken.verify(token, config.secret);
    } catch (e) {
        return false;
    }

}

function generate(data){

    const TOKEN_EXPIRATION = config.token_expiration? config.token_expiration:60;
    const TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60;

    //var data = user.dataValues;
    data.token = jsonwebtoken.sign(data, config.secret, {
        expiresIn: TOKEN_EXPIRATION_SEC
    });

    var decoded = jsonwebtoken.decode(data.token);

    data.token_exp = decoded.exp;
    data.token_iat = decoded.iat;

    return data;

}

exports.decode = function(token) {

    try{
        return jsonwebtoken.decode(token);
    } catch (e) {
        return false;
    }

}
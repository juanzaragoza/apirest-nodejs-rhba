const prefixPath = '/api';

module.exports = function (app) {
    app.use(prefixPath, require('./routes/default'));
    app.use(prefixPath, require('./routes/user'));
};
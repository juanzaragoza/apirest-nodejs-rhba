// *** absolute path configuration *** //
global.base_dir = __dirname;
global.abs_path = function(path) {
  return base_dir + path;
}
global.include = function(file) {
  return require(abs_path('/' + file));
}

// *** main dependencies *** //
var express = require('express'),
    path = require('path'),
    bodyParser = require('body-parser'),
    http = require('http'),
    ejwt = require('express-jwt'),
    common = include("controller/common");

// *** config file *** //
var config = require(path.join(__dirname,'.','config','config'));

// *** setting up our singleton model *** //
var config_mysql;
switch(process.env.NODE_ENV){
    case 'test':
        config_mysql = config.mysql.test;
        break;

    case 'prod':
        config_mysql = config.mysql.prod;
        break;

    default:
        config_mysql = config.mysql.dev;
}
include("lib/orm/mysql").setup(
    'model', 
    config_mysql.database, 
    config_mysql.user, 
    config_mysql.password, 
    {
        host: config_mysql.host, 
        port: config_mysql.port,
        define: {timestamps: false }
    }
);

// *** express instance *** //
var app = express();
app.use(bodyParser.json());       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));

/** jwt configuration **/
var orm = include('lib/orm/mysql');

var isRevokedCallback = function(req, payload, done){
    if(!true){
        return done({name:'UnauthorizedError'});
    } else{
        return done(null,false);
    }
};

if(config.secured) {
    app.use(ejwt({ secret: config.secret, isRevoked: isRevokedCallback}).unless({path: [/(\/api\/auth\/?)$/,/(\/api\/(?!\auth))/, /\/apidoc\/?/, /\/public\/img\/?/]}));
    app.use(function (err, req, res, next) {
        console.log(err.name);
        if (err.name === 'UnauthorizedError') {
            return common.responseUnauthorizedAccess(res,"Invalid token");
        }
    });
}

/** x-powered-by **/
app.disable('x-powered-by');

// *** config middleware *** //
app.use('/apidoc', express.static('apidoc'));
app.use('/public', express.static('public'));

// *** server config *** //
var server   = http.createServer(app);
const port = config.port || 3000;

server.listen(port, function() {
    console.log('Server running in ' + port);
});

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Vary", "Origin");
    res.header("Access-Control-Allow-Credentials", "true");
    //res.header("Access-Control-Allow-Methods", "GET, POST");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

    try {
        decodeURIComponent(req.url)
        next()
    } catch (err) {
        if (err.name === 'URIError') {
            res.status(404).json({'ERROR': "Bad url." })
        }
    }

});

// ***  routes *** //
require('./router')(app);


module.exports = app;

var filesystem = require('fs');
var models = {};
var relationships = {};
var classMethods = {};

var singleton = function singleton(){
    var Sequelize = require("sequelize");
    var sequelize = null;
    var modelsPath = "";
    this.setup = function (path, database, username, password, obj){
        modelsPath = path;

        if(arguments.length == 3){
            sequelize = new Sequelize(database, username);
        }
        else if(arguments.length == 4){
            sequelize = new Sequelize(database, username, password);
        }
        else if(arguments.length == 5){
            sequelize = new Sequelize(database, username, password, obj);
        }
        init();
    }

    this.model = function (name){
        return models[name];
    }

    this.Seq = function (instanciated = false){
        if(instanciated){
            return sequelize;
        }
        return Sequelize;
    }

    function init() {
        filesystem.readdirSync(modelsPath).forEach(function(name){
            var object = include(modelsPath + "/" + name);
            var options = object.options || {}
            var modelName = name.replace(/\.js$/i, "");
            models[modelName] = sequelize.define(modelName, object.model, options);
            if("relations" in object){
                relationships[modelName] = object.relations;
            }
            if("classMethods" in object){
                classMethods[modelName] = object.classMethods;
            }
        });
        //console.log(relationships);
        for(var name in relationships){
            var relations = relationships[name];
            for(var relName in relations){
                for(var related in relations[relName]){
                    var related = relations[relName][related];
                    models[name][relName](models[related.name],related.options);
                }
            }
        }
        //class methods
        for(var modelName in classMethods){
            var classMethod = classMethods[modelName];
            for(var methodName in classMethod){
                models[modelName][methodName] = classMethod[methodName];
            }
        }
    }

    if(singleton.caller != singleton.getInstance){
        throw new Error("This object cannot be instanciated");
    }
}

singleton.instance = null;

singleton.getInstance = function(){
    if(this.instance === null){
        this.instance = new singleton();
    }
    return this.instance;
}

module.exports = singleton.getInstance();